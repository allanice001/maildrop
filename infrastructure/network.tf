# Create a VPC for everything to live in

resource "aws_vpc" "maildrop" {
  cidr_block = "172.17.0.0/16"
  tags {
    Name = "maildrop2vpc"
  }
}

# Create a public facing subnet for redis, fargate, lambda

data "aws_availability_zones" "available" {}

resource "aws_subnet" "public" {
  cidr_block = "172.17.0.0/20"
  vpc_id = "${aws_vpc.maildrop.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  map_public_ip_on_launch = true
  tags {
    Name = "maildrop2subnet"
  }
}

output "lambda_subnet" {
  value = "${aws_subnet.public.id}"
}

# Internet gateway for outbound traffic

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.maildrop.id}"
  tags {
    Name = "maildrop2gw"
  }
}

# Route all outbound traffic through the gateway

resource "aws_route" "internet_access" {
  route_table_id = "${aws_vpc.maildrop.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.gw.id}"
}
