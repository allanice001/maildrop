# S3 bucket for hosting the client React app

resource "aws_s3_bucket" "client" {
  bucket = "${var.domain}"
  acl = "public-read"
  policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[{
	"Sid":"PublicReadGetObject",
        "Effect":"Allow",
	  "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::${var.domain}/*"]
    }
  ]
}
EOF
  website {
    index_document = "index.html"
    error_document = "index.html"
  }
}

# www.domainname.com redirects to https://domainname.com

resource "aws_s3_bucket" "www" {
  bucket = "www.${var.domain}"
  acl = "public-read"
  policy = <<EOF
{
  "Version":"2012-10-17",
  "Statement":[{
	"Sid":"PublicReadGetObject",
        "Effect":"Allow",
	  "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::www.${var.domain}/*"]
    }
  ]
}
EOF
  website {
    redirect_all_requests_to = "https://${var.domain}"
  }
}